# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the example of using [zmq](http://zeromq.org/) library. 
* It's server which receives messages from several clients (sensors for instance), processes messages, counts number of messages.
* Version 1.0

### How do I get set up? ###

* Install [zmq](http://zeromq.org/) library
* Go to the src/ directory and run make
* Run zmqs application
* Send signal USR1 to print counters

### Who do I talk to? ###

* Contact miroslav.merinsky@gmail.com