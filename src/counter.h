/**
 * @file counter.h
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Header for counter.c
 */
#ifndef __COUNTER_H__
#define __COUNTER_H__

#define FLAG_CLEAR_COUNTER 1 /**< clear counter */

/**
 * @brief Increases counter
 *
 * @param token Token
 * @return Number 0 if success, otherwise -1
 */
extern int increaseCounter(const char *token);

/**
 * @brief Prints counters to standard output
 *
 * @param clear Flag indicates if couters should be cleared
 */
extern void printCounters(int clear);

/**
 * @brief Frees allocated memory in heap
 */
extern void cleanCounters();

#endif /* __COUNTER_H__ */
