/**
 * @file process.c
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Process messages functions
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "server.h"
#include "process.h"


static int processMessageTest1(const struct MsgTypeTest1 *msg);
static int processMessageTest2(const struct MsgTypeTest2 *msg);
static int processMessageTest3(const struct MsgTypeTest3 *msg);


/**
 * ****************************************************************************
 * Definition of global functions
 * ****************************************************************************
 */

/**
 * @brief Processes message
 *
 * @param token Device token
 * @param messageType Message type
 * @param message Message
 * @return Result or -1 if error has occured
 */
int processMessage(const char *token, int messageType, const void *message)
{
	assert(token != NULL && strlen(token) == MSG_TOKEN_LENGTH);
	assert(messageType >= MSG_TYPE_TEST1 && messageType <= MSG_TYPE_TEST3);
	assert(message != NULL);

	switch (messageType) {
		case MSG_TYPE_TEST1:
			return processMessageTest1((struct MsgTypeTest1 *) message);
			break;

		case MSG_TYPE_TEST2:
			return processMessageTest2((struct MsgTypeTest2 *) message);
			break;

		case MSG_TYPE_TEST3:
			return processMessageTest3((struct MsgTypeTest3 *) message);
			break;
	}

	return -1;
}

/**
 * @brief Returns message length
 *
 * @param type Message type
 * @return Message length or -1 if error has occured
 */
int getMessageLength(MSG_TYPE type)
{
	assert(type >= MSG_TYPE_TEST1 && type <= MSG_TYPE_TEST3);

	switch (type) {
		case MSG_TYPE_TEST1:
			return sizeof(struct MsgTypeTest1);
			break;

		case MSG_TYPE_TEST2:
			return sizeof(struct MsgTypeTest2);
			break;

		case MSG_TYPE_TEST3:
			return sizeof(struct MsgTypeTest3);
			break;
	}

	return -1;
}

/**
 * ****************************************************************************
 * Definition of local functions
 * ****************************************************************************
 */


/**
 * @brief Processes test1 message
 *
 * @param msg Message
 * @return Result or -1 if error has occured
 */
static int processMessageTest1(const struct MsgTypeTest1 *msg)
{
	// do something...

	return 1;
}

/**
 * @brief Processes test2 message
 *
 * @param msg Message
 * @return Result or -1 if error has occured
 */
static int processMessageTest2(const struct MsgTypeTest2 *msg)
{
	// do something...

	return 2;
}

/**
 * @brief Processes test3 message
 *
 * @param msg Message
 * @return Result or -1 if error has occured
 */
static int processMessageTest3(const struct MsgTypeTest3 *msg)
{
	// do something...

	return 3;
}
