/**
 * @file main.c
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Main function
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include "common.h"
#include "server.h"
#include "counter.h"

#define LISTENING_INTERFACE "lo" /**< interface on which server will listen on */
#define LISTENING_PORT 2000 /**< listening port for server */

void sig_handler(int signo);


/**
 * ****************************************************************************
 * Definition of global functions
 * ****************************************************************************
 */

/**
 * @brief Main function
 *
 * @param argc
 * @param argv
 * @return Number 0, or -1 if error has occured
 */
int main(int argc, char *argv[])
{
  	char *interface = LISTENING_INTERFACE;
  	int port = LISTENING_PORT;
  	int c;

  	// parse options
    while ((c = getopt(argc, argv, "i:p:")) != -1) {
	    switch (c) {
	      	case 'i':
	        	interface = optarg;
	        	break;

	      	case 'p':
	        	port = atoi(optarg);
	        	break;

	      	case '?':
		        if ((optopt == 'i') || (optopt == 'p')) {
		          	fprintf (stderr, "Option -%c requires an argument.\n", optopt);
		        } else if (isprint(optopt)) {
		          	fprintf (stderr, "Unknown option `-%c'.\n", optopt);
		        } else {
		          	fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
		        }
	        	return -1;

	      	default:
	        	abort();
	    }
  	}

  	// signal
  	if (signal(SIGTERM, sig_handler) == SIG_ERR) {
  		error("Can not set SIGTERM signal handler.");
  		return -1;
  	}
  	if (signal(SIGUSR1, sig_handler) == SIG_ERR) {
  		error("Can not set SIGUSR1 signal handler.");
  		return -1;
  	}

  	// bind
  	if (createServer(interface, port) < 0) {
  		error("Can not bind on %s:%d.", interface, port);
  		return -1;
  	}

  	return 0;
}

/**
 * @brief Signal handler
 *
 * @param signo Signal number
 */
void sig_handler(int signo)
{
    if (signo == SIGTERM) {
    	cleanCounters();
    } else if (signo == SIGUSR1) {
    	printCounters(FLAG_CLEAR_COUNTER);
    }
}
