/**
 * @file common.h
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Common definitions
 */
#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>

#define BUFFER_16 16 /**< buffer length 16 bytes */
#define BUFFER_32 32 /**< buffer length 32 bytes */
#define BUFFER_64 64 /**< buffer length 64 bytes */
#define BUFFER_128 128 /**< buffer length 128 bytes */
#define BUFFER_256 256 /**< buffer length 256 bytes */
#define BUFFER_512 512 /**< buffer length 512 bytes */

/**
 * @brief Prints error message
 */
#define error(args...) do { \
    	fprintf(stderr, "Error: %s (%d): ", __FILE__, __LINE__); \
    	fprintf(stderr, args); \
    	fprintf(stderr, "\n"); \
    } while(0)

/**
 * @brief Prints warning message
 */
#define warning(args...) do { \
    	fprintf(stderr, "Warning: %s (%d): ", __FILE__, __LINE__); \
    	fprintf(stderr, args); \
    	fprintf(stderr, "\n"); \
    } while(0)

/**
 * @brief Prints debug message
 */
#define debug(args...) do { \
    	fprintf(stderr, "Debug: %s (%d): ", __FILE__, __LINE__); \
    	fprintf(stderr, args); \
    	fprintf(stderr, "\n"); \
    } while(0)

#endif /* __COMMON_H__ */
