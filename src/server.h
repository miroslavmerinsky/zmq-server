/**
 * @file server.h
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Header for server.c
 */
#ifndef __SERVER_H__
#define __SERVER_H__

#define MSG_TOKEN_LENGTH 32 /**< length of message token */
#define MSG_TYPE_LENGTH 1 /**< length of message type */

/**
 * @brief Creates server on appropriate interface
 *
 * @param interface Interface name (i.e. eth0) or NULL for all interfaces
 * @param port TCP/UDP port number
 * @return Number -1 if error has occured, otherwise number 0
 */
extern int createServer(const char *interface, unsigned int port);

#endif /* __SERVER_H__ */