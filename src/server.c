/**
 * @file server.c
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Creates server using zmq
 */
#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
// getInterfaceAddress()
#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <linux/if_link.h>

#include "common.h"
#include "counter.h"
#include "process.h"
#include "server.h"


static const char *getListener(const char *interface, unsigned int port);
static const char *getInterfaceAddress(const char *interface);


/**
 * ****************************************************************************
 * Definition of global functions
 * ****************************************************************************
 */

/**
 * @brief Creates server on appropriate interface
 *
 * @param interface Interface name (i.e. eth0) or NULL for all interfaces
 * @param port TCP/UDP port number
 * @return Number -1 if error has occured, otherwise number 0
 */
int createServer(const char *interface, unsigned int port)
{
    assert(*interface != '\0'); // interface can be NULL
    assert(port > 0 && port <= 65535);

    char token[MSG_TOKEN_LENGTH + 1]; // token is used for authentication
    char buffer[BUFFER_512];
    int messageType, response, received;

    // bind
    void *context = zmq_ctx_new();
    void *responder = zmq_socket (context, ZMQ_REP);

    if (zmq_bind(responder, getListener(interface, port)) != 0) {
        error("The zmq_bind() failed.");
        return -1;
    }

    while (1) {
        // skip invalid message
        // zmq_recv() is waiting to new message
        if ((received = zmq_recv(responder, buffer, BUFFER_512, 0)) < 0)
            continue;

        // test message length
        if ((received < (MSG_TOKEN_LENGTH + MSG_TYPE_LENGTH))
            || (received > BUFFER_512)) {
            warning("Invalid length of message, length=%d.", received);
            continue;
        }

        memset(token, '\0', sizeof(token));
        strncpy(token, buffer, MSG_TOKEN_LENGTH);
        messageType = (int)buffer[MSG_TOKEN_LENGTH];

        // test message type
        if ((messageType < MSG_TYPE_TEST1) || (messageType > MSG_TYPE_TEST3)) {
            warning("Invalid message type, type=%d.", messageType);
            continue;
        }

        // test message length
        if ((received - (MSG_TOKEN_LENGTH + MSG_TYPE_LENGTH)) != getMessageLength(messageType)) {
            warning("Invalid message length, type=%d, size=%d.", messageType, received - (MSG_TOKEN_LENGTH + MSG_TYPE_LENGTH));
            continue;
        }

        // process message
        response = processMessage(token, messageType, buffer + MSG_TOKEN_LENGTH + MSG_TYPE_LENGTH);
        sprintf(buffer, "%c", (unsigned char)response);

        // counter
        (void) increaseCounter(token);

        zmq_send(responder, buffer, 1, 0);
    }

    return 0;
}


/**
 * ****************************************************************************
 * Definition of local functions
 * ****************************************************************************
 */

/**
 * @brief Gets listener
 *
 * @param interface Interface name (i.e. eth0) or * for all
 * @param port TCP/UDP port number
 * @return The listener
 */
static const char *getListener(const char *interface, unsigned int port)
{
    assert(interface != NULL && *interface != '\0');
    assert(port > 0 && port <= 65535);

    static char listener[BUFFER_64];

    sprintf(listener, "tcp://%s:%d", (*interface == '*') ? "*" : getInterfaceAddress(interface), port);

    return listener;
}

/**
 * @brief Gets interface IP address
 *
 * @param interface Interface name (i.e. eth0)
 * @return Returns address or NULL if error has occured
 */
static const char *getInterfaceAddress(const char *interface)
{
    assert(interface != NULL && *interface != '\0');

    struct ifaddrs *ifaddr, *ifa;
    int s = 0, n;
    static char host[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1) {
        error("getnameinfo() failed: %s\n", gai_strerror(s));
        return NULL;
    }

    /* walk through linked list */
    for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
        if (ifa->ifa_addr == NULL)
            continue;

        if ((strcmp(ifa->ifa_name, interface) == 0) && (ifa->ifa_addr->sa_family == AF_INET)) {
           if ((s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST)) != 0) {
                error("getnameinfo() failed: %s\n", gai_strerror(s));
                freeifaddrs(ifaddr);
                return NULL;
           }
        }
    }

    freeifaddrs(ifaddr);
    return host;
}
