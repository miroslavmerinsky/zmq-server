/**
 * @file process.h
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Header for process.c
 */
#ifndef __PROCESS_H__
#define __PROCESS_H__

#include "common.h"

/**
 * @brief The enum of message types
 */
typedef enum {
	MSG_TYPE_TEST1 = 1, /**< message type test1 */
	MSG_TYPE_TEST2, /**< message type test2 */
	MSG_TYPE_TEST3 /**< message type test3 */
} MSG_TYPE;

/**
 * @brief The structure represent test 1 message data
 */
struct MsgTypeTest1 {
	int x; /**< number of x */
	int y; /**< number of y */
	int z; /**< number of z */
};

/**
 * @brief The structure represent test 2 message data
 */
struct MsgTypeTest2 {
	int x; /**< number of x */
};

/**
 * @brief The structure represent test 3 message data
 */
struct MsgTypeTest3 {
	char name[BUFFER_16]; /**< name */
};

/**
 * @brief Processes message
 *
 * @param token Device token
 * @param messageType Message type
 * @param message Message
 * @return Result or -1 if error has occured
 */
extern int processMessage(const char *token, int messageType, const void *message);

/**
 * @brief Returns message length
 *
 * @param type Message type
 * @return Message length or -1 if error has occured
 */
extern int getMessageLength(MSG_TYPE type);

#endif /* __PROCESS_H__ */
