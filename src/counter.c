/**
 * @file counter.c
 * @author Miroslav Merinsky <miroslav.merinsky@gmail.com>
 * @date 21 Sep 2016
 * @copyright MIT
 * @brief Implementation of easy counter in linked list
 */
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <malloc.h>

#include "common.h"
#include "server.h"
#include "counter.h"

/**
 * @brief The structure represent easy linked list of counters
 */
struct list {
	char token[MSG_TOKEN_LENGTH+1]; /**< token (identification) */
	unsigned int count; /**< count */
	time_t from; /**< time when counter was reseted */
	struct list *next; /**< next counter */
};


static struct list *findToken(const char *token);
static int addCounter(const char *token);
/*static int deleteCounter(const char *token);*/


struct list *head = NULL;


/**
 * ****************************************************************************
 * Definition of global functions
 * ****************************************************************************
 */

/**
 * @brief Increases counter
 *
 * @param token Token
 * @return Number 0 if success, otherwise -1
 */
int increaseCounter(const char *token)
{
	assert(token != NULL && (strlen(token) == MSG_TOKEN_LENGTH));

	struct list *p = NULL;

	if ((p = findToken(token)) != NULL) {
		debug("Increase token %s.", token);
		p->count++;
	} else {
		debug("Adding token %s", token);
		if (addCounter(token) < 0) {
			error("Can not add new counter.");
			return -1;
		}
	}

	return 0;
}

/**
 * @brief Prints counters to standard output
 *
 * @param clear Flag indicates if couters should be cleared
 */
void printCounters(int clear)
{
	struct list *p = head;

	while (p != NULL) {
		printf("%s    %8d   %s", p->token, p->count, ctime(&(p->from)));

		if (clear) {
			p->count = 0;
			p->from = time(NULL);
		}

		p = p->next;
	}
}

/**
 * @brief Frees allocated memory in heap
 */
void cleanCounters()
{
	struct list *p = head;
	struct list *pTmp;

	while (p != NULL) {
		pTmp = p;
		p = p->next;

		free((void *)pTmp);
	}

	head = NULL;
}


/**
 * ****************************************************************************
 * Definition of local functions
 * ****************************************************************************
 */

/**
 * @brief Finds token
 *
 * @param token Token
 * @return Token or NULL if token has not been found
 */
static struct list *findToken(const char *token)
{
	assert(token != NULL && (strlen(token) == MSG_TOKEN_LENGTH));

	struct list *p = head;

	while (p != NULL) {
		if (strcmp(token, p->token) == 0) {
			return p;
		}

		p = p->next;
	}

	return NULL;
}

/**
 * @brief Adds new counter to the list
 *
 * @param token Token
 * @return Number 0 if success, otherwise -1
 */
static int addCounter(const char *token)
{
	assert(token != NULL && (strlen(token) == MSG_TOKEN_LENGTH));

	struct list *p = NULL;
	struct list *pTmp = head;

	// create
	if ((p = (struct list *) malloc(sizeof(struct list))) == NULL) {
		error("Allocation memory in heap has failed.");
		return -1;
	}

	memset(p, 0, sizeof(struct list));
	strcpy(p->token, token);
	p->count++;
	p->from = time(NULL);

	if (pTmp != NULL) {
		while (pTmp->next != NULL) {
			pTmp = pTmp->next;
		}
		pTmp->next = p;
	} else {
		// first item
		head = p;
	}

	return 0;
}

/**
 * @brief Deletes counter for given token
 *
 * @todo Create this function
 * @param token Token
 * @return Number 0 if success, otherwise -1
 */
/*
static int deleteCounter(const char *token)
{
	return -1;
}
*/